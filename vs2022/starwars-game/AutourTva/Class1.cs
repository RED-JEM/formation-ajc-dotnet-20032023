﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace decouverte_delegates
{
    delegate void Afficher(string[] args)// le quoi : la signature de votre methode, c'est un nouveau type
    
      {
         List<double> tvaList = new List<double>();
         Console.WriteLine("Entrez les TVA existantes (séparées par des espaces) :");
         string[] tvaArray = Console.ReadLine().Split(' ');

         foreach (string tva in tvaArray)
         {
            double tvaValue;
            if (double.TryParse(tva, out tvaValue))
            {
                tvaList.Add(tvaValue);
            }
         }

         Console.WriteLine("TVA saisies :");
         foreach (double tva in tvaList)
         {
            Console.WriteLine("{0} - {1} €", tva, tva.ToString("C"));
         }

         Console.WriteLine("\nFiltrage des TVA supérieures à 1.055 :");
         var filteredTvas = tvaList.Where(tva => tva > 1.055);
         foreach (double tva in tvaList)
         {
            if (filteredTvas.Contains(tva))
            {
                Console.ForegroundColor = ConsoleColor.Green;
            }
            Console.WriteLine("{0} - {1} €", tva, tva.ToString("C"));
            Console.ResetColor();
         }
     }
    
}