﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parti.api
{
    /// <summary>
    /// 
    /// </summary>
    public class Game
    {
        
        #region Public methods
        /// <summary>
        /// Demarrer une nouvelle partie
        /// Crée un nouveeau personnage
        /// </summary>
        public void Demarrer()
        {
            this.DateDebut = DateTime.Now;
        }
        #endregion

        /// <summary>
        /// Creer un point de sauvegarde 
        /// </summary>
        public void Sauvegarder()
        {
            throw new NotImplementedException();
        }


        #region Proprietes

        public List<CheckPoint> checkPoints { get; set; } = new ();
        public DateTime DateCreation { get; private set; }

        public DateTime DateDebut { get; private set;} = DateTime.Now;

        #endregion

    }
}
