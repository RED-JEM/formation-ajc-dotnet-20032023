﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parti.api
{
    /// <summary>
    /// Calsse parente des personange => vous devez creer une classe enfante pour instancier
    /// </summary>
    public internal class BasePersonnage
    {
        #region Public methodes
        public virtual void Attaquer(BasePersonnage ennemi)

        {
            if (this != ennemi)
            {
                throw new Exceptions.PersonnageIdentiqueAttenduExecption();
            }
            ennemi.PointsDeVie -= this.Force;

        }
        protected void PerdreVie(int value)
        {
            this.
            this.PointsDeVie -= value;
        }

        /// <summary>
        /// Se proteger et recuperer
        /// </summary>
        /// <param name="person"></param>
        /// <exception cref="NotImplementedException"></exception>
        public virtual void SeDefendre(BasePersonnage person)
        {
            throw new Exception.PersonnageIdentiqueAttenduException();
        }
        #endregion

        #region
        public const int DEFAUT_POINT_DE_VIE 
        {
            gggg
        }
    }
        #endregion

}

