﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parti.api.Exceptions
{
    /// <summary>
    /// A declencher quand deux personnages de jeu sont identiques
    /// </summary>
    public class PersonnageIdentiqueAttenduExecption : Exception
    {
    }
}
