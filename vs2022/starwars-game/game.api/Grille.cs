﻿using game.api.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api
{
    /// <summary>
    /// La grille est l'équivalent de la carte du monde sur lequel les personnages vont se deplacer
    /// </summary>
    public class Grille : Afficheur
    {
        /// <summary>
        /// 
        #region Attribut
        private IAfficheur afficher;
        private int Largeur;
        private int Hauteur;
        #endregion

        #region Construtor
        public Grille(Afficheur afficheur, int Largeur, int Hauteur)
        {
            this.afficher = afficheur;
            this.Largeur = Largeur;
            this.hauteur = Hauteur;
        }

        public int largeur { get => Hauteur; set => Largeur = value; }
        public int hauteur { get => Largeur; set => Largeur = value; }
        #endregion
        public void AfficherGrille()
        {
            for (int i = 0; i < Largeur; i++)
            {
                for (int j = 0; j < Hauteur; j++)
                {
                    afficher.AfficherMessage("O ");
                }
                afficher.AfficherLigne("");
            }
        }

        public void AfficherGrille(BasePersonnage personnage, List<Ennemi> ennemis)
        {
            for (int i = 0; i < Largeur; i++)
            {
                for (int j = 0; j < Hauteur; j++)
                {
                    if (i == personnage.Position.X && j == personnage.Position.Y)
                    {
                        afficher.AfficherMessage("[X]");
                    }
                    else
                    {
                        afficher.AfficherMessage("[ ]");
                    }
                }
                afficher.AfficherLigne("");
            }
        }

        public void AfficherLigne(string value)
        {
            throw new NotImplementedException();
        }

        public void AfficherMessage(string value)
        {
            throw new NotImplementedException();
        }
    }
}