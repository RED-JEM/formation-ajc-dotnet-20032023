﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api.Adapters
{
    /// <summary>
    /// Interface utilisée pour obtenir les données entrée par l'utilisateur dans l'application
    /// </summary>
    public interface ILecteur
    {
        /// <summary>
        /// Methode utilisée pour recuperer les infos saisies par l'utilisateur
        /// </summary>
        /// <returns>Les informations entrées par l'utilisateur</returns>
        string RecupererInput();
    }
}
