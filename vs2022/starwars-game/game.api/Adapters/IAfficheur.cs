﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api.Adapters
{
    /// <summary>
    /// Interface utilisée pour afficher des données
    /// </summary>
    public interface IAfficheur
    {
        /// <summary>
        /// Methode permettant d'afficher une chaine de caractères sans faire de retour à la ligne
        /// </summary>
        /// <param name="value">L'information passée à afficher</param>
        void AfficherMessage(string value);

        /// <summary>
        /// Methode pour afficher du texte sur une ligne complete et faire un retour à la ligne
        /// </summary>
        /// <param name="value"></param>
        void AfficherLigne(string value);
    }
}