﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api
{
    /// <summary>
    /// Position X et Y d'un <c>BasePersonnage</c>
    /// </summary>
    //public class Position2D
    //{
    //    #region Properties
    //    public int X { get; set; }

    //    public int Y { get; set; }
    //    #endregion
    //}
    public record Position2D(int X, int Y);
}
