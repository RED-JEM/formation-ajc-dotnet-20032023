﻿using game.api.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api
{
   
        /// <summary>
        /// Classe implémentant l'interface ILecteurInfos utilisée pour recuperer les informations saisie par l'utilisateur
        /// </summary>
    public class Lecteur : ILecteur
    {
        /// <summary>
        /// Methode pour récupérer la saisie de l'utilisateur dans la caonsole
        /// </summary>
        public string lireInfos()
        {
            return Console.ReadLine();
        }

        public string RecupererInput()
        {
            throw new NotImplementedException();
        }
    }
}
