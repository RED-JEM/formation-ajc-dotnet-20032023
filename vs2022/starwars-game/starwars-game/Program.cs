<<<<<<< HEAD
﻿
using game.api;
using game.api.Infrastructure;
using menu.api;
using starwars_game;

Random random = new Random();

Afficheur afficheur = new();
Lecteur lecteur = new();

string cheminSauvegarde = Path.Combine(Environment.CurrentDirectory, "checkpoint.json");

var game = new Game(new JsonSauvegardePartie(cheminSauvegarde), afficheur, lecteur);
BasePersonnage persoPrincipal = new PersoPrincipal(afficheur.AfficherLigne, lecteur.lireInfos);

Menu menu = new(mess =>
{
    Console.ForegroundColor = ConsoleColor.DarkGreen;
    afficheur.AfficherLigne(mess);
    //Console.WriteLine(mess);
    Console.ForegroundColor = ConsoleColor.White;
}, 
Console.ReadLine);


game.Demarrage += (Game arg1, DateTime arg2) =>
{
    afficheur.AfficherLigne("Démarrage de la partie !");
};

game.Demarrer(persoPrincipal);

// Je vais attaquer

try
{
    game.Sauvegarder();
    // Je ferme
}
catch (NotImplementedException ex)
{
    Console.ForegroundColor = ConsoleColor.Green;
    afficheur.AfficherMessage("Oops erreur !", ex.Message);
    Console.ForegroundColor = ConsoleColor.White;
}
catch(Exception ex)
{

}
finally
{
    afficheur.AfficherLigne("Execute obligatoire");
}
//catch (Exception ex)
//{
//    Console.ForegroundColor = ConsoleColor.Red;
//    Console.WriteLine("Oops erreur", ex.Message);
//    Console.ForegroundColor = ConsoleColor.White;
//}




Console.ForegroundColor = ConsoleColor.DarkGreen;

// string monTitre = "a starwars game".ToUpper();
var monTitre = "a starwars game".ToUpper();

// monTitre = monTitre + " (" + DateTime.Now.ToString() + ")";
//string format = "{0} ({1})";
//monTitre = string.Format(format, monTitre, DateTime.Now);
//Console.WriteLine(monTitre);

// Code identique
// Console.WriteLine(format, monTitre, DateTime.Now);

afficheur.AfficherLigne($"{monTitre.ToUpper()} ({DateTime.Now.ToString("yyyy.MM.dd dddd")}) !");
Console.ForegroundColor = ConsoleColor.DarkCyan;
var sousTitre = "A zelda copy cat";
afficheur.AfficherLigne(sousTitre);
Console.ForegroundColor = ConsoleColor.White;


void AfficherMenu()
{
    //string[] menu = { // new string[] 
    //    "1. Nouvelle partie",
    //    "2. Charger partie",
    //    "0. Quitter"
    //};

    //foreach (var item in menu)
    //{
    //    Console.WriteLine(item);
    //}

    var listMenu = Enum.GetValues(typeof(MenuItemType));
    foreach (var item in listMenu)
    {
        string itemTransforme = item.ToString().Replace("_", " ");
        Console.WriteLine("{0}: {1}", (int)item, itemTransforme);
    }

    //Type type = typeof(string);
    //foreach(var item in type.GetMethods())
    //{
    //    Console.WriteLine(item);
    //}
}

int ChoisirMenu()
{
    int menuItem = 0;

    do
    {
        AfficherMenu();

        afficheur.AfficherLigne("Ton choix ?");
        menuItem = int.Parse(Console.ReadLine()); // TODO: Voir pour utiliser un try parse => ou gestion des exceptions
        // const int DEMARRER_PARTIE_INDEX = 1;
        //        MenuItemType monChoix = MenuItemType.Quitter;

        MenuItemType monChoix = (MenuItemType)menuItem;
        switch (monChoix)
        {
            case MenuItemType.Demarrer_Partie:
                {
                    DemarrerPartie(); // Ici, exceptions ?
                }
                break;

            case MenuItemType.Charger_Partie:
                {
                    ChargerPartie();
                }
                break;

            // TODO: il manque le default ?
        }
    } while (menuItem != 0);

    return menuItem;
}

void DemarrerPartie()
{
    afficheur.AfficherLigne("Ton prénom stp ?");
    var prenom = Console.ReadLine();

    // DateTime maVraiDate;
    bool dateValide = false;
    do
    {
        afficheur.AfficherLigne("Ta date de naissance stp ?");
        var dateDeNaissance = Console.ReadLine();

        if (DateTime.TryParse(dateDeNaissance, out var maVraiDate))
        {
            var comparaisonDates = DateTime.Now - maVraiDate;
            int nbAnnees = (int)(comparaisonDates.TotalDays / 365);
            afficheur.AfficherLigne($"Ton prénom est bien ? {prenom}, et tu es âgé de {nbAnnees} années ?");

            dateValide = true;
            int ageSaisi = DemanderAge();
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            afficheur.AfficherLigne("Erreur de saisie de date, ré-essaie stp !");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
    while (!dateValide);
}

int DemanderAge()
{
    bool ageValide = false;
    int vraiAge = 0;

    while (!ageValide)
    {
        afficheur.AfficherLigne("Ton age stp ?");
        var age = Console.ReadLine();

        ageValide = int.TryParse(age, out vraiAge);

        if (ageValide)
        {
            ageValide = vraiAge > 13;
        }
    }

    return vraiAge;
=======
﻿// See https://aka.ms/new-console-template for more information

using System.Net.Cache;

Console.ForegroundColor = ConsoleColor.DarkGreen;
string monTitre = " a starwars game".ToUpper();

////string sousTitre = " A copycat of zelda ";
//Console.ForegroundColor = ConsoleColor.Yellow;

//Console.ForegroundColor = ConsoleColor.White;

//// Demander le nom de l'utilisateur
//Console.WriteLine("Entrez votre nom : ");
//string nom = Console.ReadLine();

//// Demander le prénom de l'utilisateur
//Console.WriteLine("Entrez votre prénom : ");
//string prenom = Console.ReadLine();

// Demander la date de naissance de l'utilisateur
//Console.WriteLine("Entrez votre date de naissance (au format jj/mm/aaaa) : ");
//DateTime dateNaissance = DateTime.Parse(Console.ReadLine());
//int age = ((DateTime.Now - dateNaissance).Days)/365;

Console.WriteLine($"{monTitre.ToUpper()} ({DateTime.Now.ToString("yyyy.MM.dd dddd")}) !");
Console.ForegroundColor = ConsoleColor.DarkCyan;

var sousTitre = "A zelda copy cat";
Console.WriteLine(sousTitre);
Console.ForegroundColor = ConsoleColor.White;


// tant que QuitJeu faux
bool QuitJeu = false;
while (!QuitJeu)
{
    Console.WriteLine("Menu:");
    Console.WriteLine("1. Nouvelle partie");
    Console.WriteLine("2. Charger partie");
    Console.WriteLine("3. Quitter");

    Console.Write("Choisissez une option: ");

    string choix = Console.ReadLine();



    // traitement du choix du joueur
    switch (choix)
    {
        case "1":
            NouvellePartie();
            break;
        case "2":
            ChargerPartie();
            break;
        case "3":
            QuitJeu = true;
            break;
        default:
            Console.WriteLine("Option invalide");
            break;
    }
}
void NouvellePartie()
{
    Console.Write("Entrez votre prénom: ");
    string prenom = Console.ReadLine();

    Console.Write("Entrez votre date de naissance (jj/mm/aaaa): ");
    string DateNaissance = Console.ReadLine();
    DateTime DateNaissance_Date = DateTime.ParseExact(DateNaissance, "dd/MM/yyyy", null);

    int age = 0;
    bool validAge = false;

    while (!validAge)
    {
        Console.Write("Entrez votre âge: ");
        string age_String = Console.ReadLine();
        if (int.TryParse(age_String, out age))
        {
            DateTime today = DateTime.Today;
            int calculAge = today.Year - DateNaissance_Date.Year;
            if (DateNaissance_Date > today.AddYears(-calculAge)) calculAge--;
            if (age == calculAge && age > 13)
            {
                validAge = true;
            }
            else
            {
                Console.WriteLine("Âge invalide. Vous devez avoir au moins 13 ans et votre âge doit correspondre à votre date de naissance.");
            }
        }
        else
        {
            Console.WriteLine("Âge invalide. Entrez un nombre entier.");
        }
    }

    // Démarrez la partie avec les informations du joueur
>>>>>>> 9379c00245d2bfaf0b0c8744d2fee3d33d464cfd
}

void ChargerPartie()
{
<<<<<<< HEAD

}


ChoisirMenu();
PreparerEnnemis();


void PreparerEnnemis()
{
    //string[] nomAs =
    //{
    //    "Dark Vador",
    //    "Boba fet",
    //    "Empereur Palpatine"
    //};
    // string[][] tableauTableau; // tableau avec des sous tableaux de dimension différentes
    //var item = tableauTableau[0][0];

    string[,] multiDimensionnel = new string[2, 3];
    var item = multiDimensionnel[0, 0];

    //noms[0] = "Darth Vader";

    List<string> noms = new List<string>()
    {
        "Dark Vador",
        "Boba fet",
        "Empereur Palpatine",
    };
    //noms.Add("Droide");
    //noms.Remove("Droide");

    //var enemi = noms[0];
    //int index = noms.IndexOf("Boba fet");
    //enemi = noms[index];


    //List<int> powers = new List<int>()
    //{
    //    1, 2, 5, 10, 20
    //};

    //var sum = powers.Sum(); // Linq
    //var first = powers.Min();

    //var min = nomAs.Min();
    DemandeAjoutEnnemisParUtilisateur(noms);

    var query = from enem in noms
                let eneMaj = enem[0].ToString().ToUpper() + enem.Substring(1).ToLower()
                let premierCar = enem[0]
                let monObjet = new { Maj = eneMaj, Initial = enem }
                where enem.StartsWith("D") || enem.EndsWith("D")
                orderby enem descending
                select monObjet;

    foreach (var enemy in query)
    {
        afficheur.AfficherLigne(enemy.Maj);
        afficheur.AfficherLigne(enemy.Initial);
    }

    //var monObjALaVolee = new // On peut créer un objet sans type, à la volée (il a un type anonyme)
    //{
    //    Prenom = "Anakin"
    //};
    //Console.WriteLine(monObjALaVolee.Prenom);

    // Linq avec methods
    //var query2 = noms.Where(item => item.StartsWith("D") && item.EndsWith("D"))
    //                 .OrderByDescending(item => item)
    //                 .Select(item => item);

    //query = query.Where(item => item.EndsWith("E"));


}

void DemandeAjoutEnnemisParUtilisateur(List<string> lesEnnemis)
{
    const string ARRET_SAISIE = "STOP";
    string valeurSaisie = "";
    bool demandeArret = false;

    do
    {
        afficheur.AfficherLigne("Nouvel ennemi ? (STOP pour arrêter)");
        valeurSaisie = Console.ReadLine();

        //var index = lesEnnemis.IndexOf(valeurSaisie);
        //if( index >= 0)
        //{

        //}
        // if (! lesEnnemis.Exists()) => sera vu avec la notion de delegués
        demandeArret = valeurSaisie == ARRET_SAISIE;

        if (!demandeArret && !lesEnnemis.Contains(valeurSaisie) )
        {
            lesEnnemis.Add(valeurSaisie);
        }

    } while (! demandeArret);
    
}








//void Calculer(int val1, int val2, out int result)
//{
//    {
//        int test = 0; //durée de vie des accolades
//    }

//    result = val1 + val2;
//}

//// int resultat = 22;
//int un = 1;
//int deux = 2;
//Calculer(un, deux, out var resultat);
//Console.WriteLine(resultat);


=======
    Console.WriteLine("Charger partie :");
    // Code pour charger une partie existante
}









int CalculerAge(DateTime dateNaissance)
{
    DateTime now = DateTime.Today;
    int age = now.Year - dateNaissance.Year;

    if (dateNaissance > now.AddYears(-age))
        age--;

    return age;
}


//// DateTime maVraiDate;
//bool dateValide = false;
//do
//{
//    Console.WriteLine("Ta date de naissance stp ?");
//    var dateNaissance = Console.ReadLine();

//    if (DateTime.TryParse(dateNaissance, out var maVraiDate))
//    {
//        var comparaisonDates = DateTime.Now - maVraiDate;
//        int nbAnnees = (int)(comparaisonDates.TotalDays / 365);
//        Console.WriteLine($"Ton prénom est bien ? {prenom}, et tu es âgé de {nbAnnees} années ?");
//        dateValide = true;
//    }
//    else
//    {
//        Console.ForegroundColor = ConsoleColor.DarkRed;
//        Console.WriteLine("Erreur de saisie de date, ré-essaie stp !");
//        Console.ForegroundColor = ConsoleColor.White;


//    }
//}
//while (!dateValide);

//Console.WriteLine(" ton prénom est bien " + prenom + " et tu es agé de " + age + " ans ");

////var monTitre = ("a stars wars!". ToUpper());
//string format = " {0} ({0})";
//monTitre = string.Format(format, monTitre, DateTime.Now);
//sousTitre = string.Format(format, sousTitre);


//monTitre = monTitre + " (" + DateTime.Now.ToString() + ")";

//Code identique
//Console.WriteLine(format, monTitre, DateTime.Now);

//Console.WriteLine($"({monTitre.ToUpper()}) ({DateTime.Now.ToString("dd/MM/yyyy")})");
//Console.WriteLine($"({sousTitre.ToLower()})");

////Console.WriteLine(format, sousTitre);



//Console.ForegroundColor = ConsoleColor.White;

//Console.WriteLine("Hello, World!");

void Calculer(int val1, int val2, out int result)
{
    {
        int test = 0; //durée de vie des accolades
    }

    result = val1 + val2;
}

// int resultat = 22;
int un = 1;
int deux = 2;
Calculer(un, deux, out var resultat);
Console.WriteLine(resultat);
>>>>>>> 9379c00245d2bfaf0b0c8744d2fee3d33d464cfd
