﻿using game.plus.api;
using territoire.api;

var ennemi = new Ennemi("Dark vador");

void DeplacementDansLaForet(Position position)
{
    Console.WriteLine("Je suis dans la jungle {0} {1}");

}

DeplacementDansLaForet(new(1, 2));
DeplacementDansLaForet(new(1, 3));

/// -----------------
/// Lambdas
/// -----------------

SePlacerDansUnLieu fonctionPlacementSuivantPosition = pos => Console.WriteLine("Je suis perdu ");

SePlacerDansUnLieu jeSuisLeDelegueBis = pos =>
{
	Console.WriteLine("Je suis perdu ");
};
jeSuisLeDelegueBis(new(2, 5));
jeSuisLeDelegueBis(new(2, 15));
ennemi.SeDeplacer(jeSuisLeDelegueBis);

ennemi.SeDeplacer(position =>
{
	Console.WriteLine("Ou suis-je ?");
});

ennemi.SeDeplacer(poss => Console.WriteLine("Ou suis-je ?"));

/// Fin lambda
/// 



ennemi.SeDeplacer(DeplacementDansLaForet);


var jungle = new Jungle();
var savane = new Savane();

//ennemi.SeDeplacer(jungle.SePlacerACoteArbre);
//ennemi.SeDeplacer(savane.SePlacerACoteElephant);

//SePlacerDansUnLieu jeSuisLeDelegue = jungle.SePlacerACoteArbre; /// c'est pas une lambda
//jeSuisLeDelegue(1, 2);



Console.ReadLine();